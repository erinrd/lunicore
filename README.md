### Codestack
 *  MySQL	Database
 *  Nodejs	Web server
 *  React	Front end
	
### Package Managers
 *  npm		bundeled with Nodejs
 *  yarn	does what npm does essencially, newer
	
### Packages (yarn)
Back end:
 *  express             the actual server
 *  mysql2	            the actual database with async support
 *  cors	            the bridge between server <-> database
 *  body-parser         used to parse bodies in POST requests
Front end:
 *  react-router-dom	for routing to correct endpoints
 *  axios				to send HTTP requests from frontend to backend
	
### Process
To set up the project i use node.js and npm to get the server running. Then I fetch all my package dependencies by using only yarn. Essencially only use npm for "npm init" and "npx create-react-app" and yarn to do the rest. This is how i started the project:

```
> mkdir carserver
> cd carserver
> npm init -y
> yarn add express mysql2 cors
> cd ..
> mkdir carfrontend
> cd carfrontend
> npx create-react-app
> mv ./.git/ ..
```

I am using MySQL workbench latest build to work with my database. I re-wrote the JSON by using notepad++ macro to an SQL insertion script. Would the data be bigger, I would have parsed it with Python. From here, I simply started coding and came up with the results in this repository.

### How to run backend
Do this to run the backend. MySQL is hardcoded to my local DB, so that needs to be changed on another machine...
```
> cd carserver
> node index.js
```

### How to run the frontend
Do this to run the frontend in development build.
```
> cd carfrontend
> yarn start
```