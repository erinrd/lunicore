const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');

// Routing constants
const ENDPOINT_ROOT       = '/';
const ENDPOINT_EMPLOYEES  = '/employees';
const ENDPOINT_CARMODELS  = '/carmodels';
const ENDPOINT_TOTAL_SALES = '/total_sales';

// MySQL Queries
const ALL_EMPLOYEES_QUERY = 'SELECT * FROM employees';
const ALL_CARMODELS_QUERY = 'SELECT * FROM carmodels';
const SALE_LISTINGS_QUERY = 'SELECT * FROM sales';
const insert_car = (brand, model, price) => {
    const str = 'INSERT INTO carmodels\n' + 
    '(brand, model, price)\n' + 
    'VALUES\n' +
    '("' + brand + '", "' + model + '", "' + price + '");'
    return str;
}
const delete_car_id = (id) => ('DELETE FROM carmodels WHERE id = ' + id + ';')
const delete_car_model = (brand, model) => ('DELETE FROM carmodels WHERE brand = "' + brand + '"\n AND model = "' + model + '";')

// Debug (verbose) printing!
const verbose = true;
const verblog = (_) => {
    if(verbose) {
        console.log(_)
    }
}
const h1 = (_) => {return '<h1>' + _ + '</h1>'}

// Lets set up and start the server!
const app  = express();

// This right here initialize and establish a connection to the MySQL server
// I am using a connection pool with 3 connection so that i can do all
// nessecary queries in parallell.
// Using an unsafe password just to demo...
const pool = mysql.createPool({
    connectionLimit: 3,
    host: 'localhost',
    user: 'root',
    password: 'eriknord',
    database: 'carshop_test'
})

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get(ENDPOINT_ROOT, (req, res) => {
    verblog("GET /  request reieved...");
    res.send(h1('Hello from server') + 
    '<p>Theese are the endpoints:<br />' + 
    '<a href="/employees">employees</a><br />' + 
    '<a href="/carmodels">carmodels</a><br />' + 
    '<a href="/total_sales">total_sales</a>');
});

app.get(ENDPOINT_EMPLOYEES, (req, res) => {
    verblog("GET /employees request reieved...");
    pool.query(ALL_EMPLOYEES_QUERY, (error, results) => {
        if(error) {
            return res.send(error)
        }
        else {
            return res.json({
                employees: results
            })
        }
    });
});

app.get(ENDPOINT_CARMODELS, (req, res) => {
    verblog("GET /carmodels  request reieved...");
    pool.query(ALL_CARMODELS_QUERY, (error, results) => {
        if(error) {
            return res.send(error)
        }
        else {
            return res.json({
                carmodels: results
            })
        }
    });
});

/**
 * This function will take jsons passed as arguments and with nessecary
 * manipulations of the data, a json object will be returned containing all
 * employees and their total sales.
 * 
 * This function assumes that all fields match those excpected from the given
 * data.json file for this case. A function that finds the absolute fields
 * could be made to make this way more general. That would be too much work
 * for this case though.
 * 
 * @param {*} jsonSales     json representation of sales from db
 * @param {*} jsonCars      json representation of cars from db
 * @param {*} jsonEmployees joson representetaion of employees from db
 * 
 * @return {json} a json object with all employees and their accumalated
 *                sales listed as a parameter.
 */
const sumAllSales = (jsonSales, jsonCars, jsonEmployees) => {
    const json2array = (_) => {return JSON.parse(JSON.stringify(_))};
    const sales = json2array(jsonSales);
    const cars = json2array(jsonCars);
    const employees = json2array(jsonEmployees);

    const getSaleValue = (sales_listing) => {
        const seller = sales_listing.employee_id;
        const index  = sales_listing.carmodel_id - 1;
        const value  = cars[index].price
        return [seller, value];
    }

    var saleSums = new Array(employees.length).fill(0); // To avoid NaN in calcs

    sales.forEach(element => {
        const sales_listing = getSaleValue(element);
        const seller_index = sales_listing[0] - 1;
        const car_value = sales_listing[1];
        saleSums[seller_index] = saleSums[seller_index] + car_value;
    });

    const data = jsonEmployees.map(employee => {
        const index = employee.id - 1;
        const sum   = saleSums[index];
        employee.sales = sum;
        return employee;
    });
    return data;
}

async function getQuery(query_string) {
    const result = await pool.query(query_string);
    return result[0];
}

app.get(ENDPOINT_TOTAL_SALES, async (req, res) => {
    verblog("GET /total_sales request reieved...");
    // By letting the callback function be async, I can start three parallel queries
    // and await them all before I return the callback function.
    let queries = [SALE_LISTINGS_QUERY, ALL_CARMODELS_QUERY, ALL_EMPLOYEES_QUERY].map(getQuery)
    const sales      = await queries[0];
    const cars       = await queries[1];
    const employees  = await queries[2];

    const data = sumAllSales(sales, cars, employees);
    return res.json({
        "total_sales": data
    });
});



//This function will check and see if the data is good enough to be passed to
//the database. I feel like I need this to bridge the gap between SQL <-> JS.
const legitCarInput = (brand, model, price) => {
    const lengths = brand.length < 45 && model.length < 45;
    const number  = price === parseInt(price, 10);
    return lengths && number;
}

app.post(ENDPOINT_CARMODELS, (req, res) => {
    verblog("POST /carmodels request reieved...")
    const brand = req.body.brand;
    const model = req.body.model;
    const price = parseInt(req.body.price);
    if(legitCarInput(brand, model, price)) {
        const INSERT_QUERY = insert_car(brand, model, price);
        pool.query(INSERT_QUERY, (error, results) => {
            if(error) {
                return res.send(error)
            }
            else {
                return res.json({
                    carmodels: results
                })
            }
        });
    } else {
        res.send("ERROR LOL")
        console.log("Was not legit input")
    }
});

app.delete(ENDPOINT_CARMODELS, (req, res) => {
    const DELETE_QUERY = delete_car_id(req.body.id);
    verblog("Deleting with id: " + req.body.id);
    verblog("This is the query: ")
    verblog(DELETE_QUERY);
    pool.query(DELETE_QUERY, (error, results) => {
        if(error) {
            return res.send(error)
        } else {
            return res.json({
                carmodels: results
            })
        }
    });
});

app.listen(4000, () => {
    console.log('Server listening on port 4000');
})
