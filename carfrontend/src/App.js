import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import axios from 'axios';

import Header from './components/layout/Header';
import AddCarField from './components/AddCarField';
import Employees from './components/pages/Employees';
import CarModels from './components/pages/CarModels';
import TotalSales from './components/pages/TotalSales';
import Home from './components/pages/Home';
import './App.css';

const EXPRESS_ADDRESS = 'http://192.168.1.18';
const EXPRESS_PORT    = '4000';
const EXPRESS_SERVER  = EXPRESS_ADDRESS + ':' + EXPRESS_PORT;
const EXPRESS_EMPLOYEES = EXPRESS_SERVER + '/employees';
const EXPRESS_CARMODELS = EXPRESS_SERVER + '/carmodels';
const EXPRESS_TOTALSALES = EXPRESS_SERVER + '/total_sales';

export default class App extends Component {

  // I want to make a state here that contains what JSON object to display into
  // the current page. Then by routing I want the app to appropiately fetch the
  // JSON needed for that display

  state = {
    employees: [],
    carmodels: [],
    total_sales: []
  }

  // HEAVY fucntion extraction use here because I want to be able to specifically
  // read the parts from DB that I need in the moment.
  readEmployees() {
    axios.get(EXPRESS_EMPLOYEES)
      .then(res => this.setState({ employees: res.data.employees}))
  }

  readCarmodels() {
    axios.get(EXPRESS_CARMODELS)
      .then(res => this.setState({ carmodels: res.data.carmodels}))
  }

  readSales() {
    axios.get(EXPRESS_TOTALSALES)
      .then(res => this.setState({ total_sales: res.data.total_sales}))
  }

  readDB() {
    this.readEmployees();
    this.readCarmodels();
    this.readSales();
  }

  componentDidMount() {
    this.readDB();
  }
  
  submition = (car) => {
    const newCar = {
      brand: car.brand,
      model: car.model,
      price: car.price
    }
    axios.post(EXPRESS_CARMODELS, newCar)
      .then((response) => {
        this.setState({ carmodels: [...this.state.carmodels, newCar] });
      }).then(() => this.readCarmodels());
  }

  deletion = (id) => {
    axios.delete(EXPRESS_CARMODELS, {
      data: {
        id: id
      }
    }).then(() => this.readCarmodels());
  }

  render() {
    return (
      <Router>
        <div className="App">
          <div className="container">
            <Header />
            <Route path="/carmodels" render={props => (
              <AddCarField submition={this.submition} />
            )} />

            <Route exact path="/" component={Home} />

            <Route path="/employees" render={props => (
              <React.Fragment>
                <table><tr><th>{'ID#'}</th><th>{'Name'}</th></tr>
                <Employees employees={this.state.employees} />
                </table>
              </React.Fragment>
            )} />

            <Route path="/carmodels" render={props => (
              <React.Fragment>
                <table><tr><th>{'Brand'}</th><th>{'Model'}</th><th>{'Price'}</th></tr>
                <CarModels carmodels={this.state.carmodels} deletion={this.deletion} />
                </table>
              </React.Fragment>
            )} />

            <Route path="/total_sales" render={props => (
              <React.Fragment>
                <table><tr><th>{'ID#'}</th><th>{'Name'}</th><th>{'Sales'}</th></tr>
                <TotalSales total_sales={this.state.total_sales} />
                </table>
              </React.Fragment>
            )} />

          </div>
        </div>
      </Router>
    )
  }
}
