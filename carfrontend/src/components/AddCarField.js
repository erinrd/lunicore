import React, { Component } from 'react'

export default class AddCarField extends Component {

    state = {
        brand: '',
        model: '',
        price: ''
    }

    reaction = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }

    submition = (event) => {
        event.preventDefault();
        this.props.submition(this.state);
        this.setState({ 
            brand: '',
            model: '',
            price: ''
        });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.submition} className="addform">
                    <input 
                        type="text"
                        name="brand"
                        placeholder="Add car brand"
                        value={this.state.brand}
                        onChange={this.reaction}
                        className="addfield"
                    />
                    <input
                        type="text"
                        name="model"
                        placeholder="Add car model"
                        value={this.state.model}
                        onChange={this.reaction}
                        className="addfield"
                    />
                    <input 
                        type="text"
                        name="price"
                        placeholder="Price of this car"
                        value={this.state.price}
                        onChange={this.reaction}
                        className="addfield"
                    />
                    <input 
                        type="submit"
                        name="carsubmit"
                        value="Submit Car Model"
                        className="addbutton"
                    />
                </form>
            </div>
        )}
}
