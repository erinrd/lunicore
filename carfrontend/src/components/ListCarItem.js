import React, { Component } from 'react'

export default class ListItem extends Component {
    render() {
        return (
            <tr className="listitem">
                <td className="listcarstring">{ this.props.brand }</td>
                <td className="listcarstring">{ this.props.model }</td>
                <td className="listcarprice">{ this.props.price }</td>
                <button className="deletion" onClick={this.props.deletion.bind(this, this.props.id)}>x</button>
            </tr>
        )
    }
}
