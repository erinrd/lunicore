import React, { Component } from 'react'

export default class ListSalesItem extends Component {
    render() {
        return (
            <tr className="listitem">
                <td className="listemployeeid">{ this.props.id }</td>
                <td className="listemployeename">{ this.props.name }</td>
                <td className="listemployeesales">{ this.props.sales }</td>
            </tr>
        )
    }
}
