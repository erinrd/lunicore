import React from 'react';
import { Link } from 'react-router-dom';

export default function Header() {
    return (
        <header>
            <h1>> Lunicore carshop</h1>
            <ul>
                <li><Link to="/">Home</Link></li>
                <li><Link to="/employees">Employees</Link></li>
                <li><Link to="/carmodels">Car models</Link></li>
                <li><Link to="/total_sales">Total sales</Link></li>
            </ul>
        </header>
    )
}