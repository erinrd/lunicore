import React, { Component } from 'react';
import ListCarItem from '../ListCarItem';

export default class CarModels extends Component {
    render() {
        return this.props.carmodels.map((e) => (
        <ListCarItem key={e.id} id={e.id} deletion={this.props.deletion} brand={e.brand} model={e.model} price={e.price} />
        ));
    }
}
