import React, { Component } from 'react'

export default class Home extends Component {
    componentDidUpdate() {
        console.log("UPDATE FROM HOME")
    }
    render() {
        return (
            <React.Fragment>
                <h1>Lunicore carshop web-app case</h1>
                <p>This case demonstrates how a stack of MySQL, Express, React, Node
                    can be used in order to set up a simple web-app for car
                    salesmanship. Everything should function fine enough. The backend
                    does everything accordnig to specification. The front end is
                    lacking the login functionality. I simply did not have time to
                    do it during the week.
                </p>
                <br ></br>
                <p>
                    Use the links in the header above to reach the endpoints of the
                    backend server. When you go to Car models, you will be able to
                    add and remove cars to and from the database. The database is
                    running on MySQL. I regret that decision, because MySQL is the
                    only part of this project that does not run on JavaScript. If I
                    were to re-do this project I would for sure use MongoDB even though
                    sources on the internet claim it to be over-hyped.
                </p>
            </React.Fragment>
        )
    }
    
}
