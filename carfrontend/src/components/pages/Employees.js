import React, { Component } from 'react';
import ListSalesItem from '../ListSalesItem';

export default class Employees extends Component {
    render() {
        return this.props.employees.map((e) => (
        <ListSalesItem key={e.id} id={e.id} name={e.name} />
        ));
    }
}
