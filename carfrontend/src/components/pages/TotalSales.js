import React, { Component } from 'react';
import ListSalesItem from '../ListSalesItem';

export default class TotalSales extends Component {
    render() {
        return this.props.total_sales.map((e) => (
        <ListSalesItem key={e.id} id={e.id} name={e.name} sales={e.sales} />
        ));
    }
}
